ARG REPO
ARG TAG

FROM ${REPO}:${TAG}

RUN apt-get update \
 && apt-get install -y --no-install-recommends less postgresql-client python3 python3-psycopg2 \
                    python3-requests zstd iproute2 \
 && apt install -y -o Dpkg::Options::="--force-confnew" libjpeg62 libpng16-16 libxrender1 \
        libfontconfig1 libxext6 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
